package model

import "encoding/xml"


type RootFiles struct {
	XMLName xml.Name `xml:"container"`
	RootFiles rootFiles `xml:"rootfiles"`
}

type rootFiles struct {
	RootFile rootFile `xml:"rootfile"`
}

type rootFile struct {
	MediaType  string `xml:"media-type,attr"`
	FullPath  string `xml:"full-path,attr"`
}