package main

import (
	"Polarr/model"
	"archive/zip"
	"encoding/xml"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	files, err := filepath.Glob(`G:\Books 2019\Removed from device\*`)
	if err != nil {
		log.Fatal(err)
	}
	for _, file := range files {
		if !strings.HasSuffix(file, ".epub") {
			continue
		}
		fmt.Println("Opening file:", file)
		unzipEPUBFile(file)
	}
}

func unzipEPUBFile(filename string) {
	files, err := Unzip(filename, "temp")
	if err != nil {
		log.Fatal(err)
	}

	checkIfValidEPUBInTemp(files)
	defer removeTempFiles(files)
}

func checkIfValidEPUBInTemp(files []string) {

	var mimeTypePresent bool
	var metaContainerPresent bool


	for _, file := range files {
		if file == `temp\mimetype` {
			mimeTypePresent = true
		}
		if file == `temp\META-INF\container.xml` {
			metaContainerPresent = true
		}
	}

	if mimeTypePresent && metaContainerPresent {
		fmt.Println("Real EPUB!")
	} else {
		fmt.Println("Phony")
	}
}

// Unzip will decompress a zip archive, moving all files and folders
// within the zip file (parameter 1) to an output directory (parameter 2).
func Unzip(fileName string, destination string) ([]string, error) {

	var filenames []string

	reader, err := zip.OpenReader(fileName)
	if err != nil {
		return filenames, err
	}
	defer reader.Close()

	for _, file := range reader.File {

		rc, err := file.Open()
		if err != nil {
			return filenames, err
		}
		defer rc.Close()

		// Store filename/path for returning and using later on
		fpath := filepath.Join(destination, file.Name)

		// Check for ZipSlip. More Info: http://bit.ly/2MsjAWE
		if !strings.HasPrefix(fpath, filepath.Clean(destination)+string(os.PathSeparator)) {
			return filenames, fmt.Errorf("%s: illegal file path", fpath)
		}

		filenames = append(filenames, fpath)

		if file.FileInfo().IsDir() {

			// Make Folder
			os.MkdirAll(fpath, os.ModePerm)

		} else {

			// Make File
			if err = os.MkdirAll(filepath.Dir(fpath), os.ModePerm); err != nil {
				return filenames, err
			}

			outFile, err := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, file.Mode())
			if err != nil {
				return filenames, err
			}

			_, err = io.Copy(outFile, rc)

			// Close the file without defer to close before next iteration of loop
			outFile.Close()

			if err != nil {
				return filenames, err
			}

		}
	}

	return filenames, nil
}

func removeTempFiles(filenames []string) {
	//for _, file := range filenames {
	//	fmt.Println(file)
	//	os.Remove(file)
	//}

	os.RemoveAll("./temp")
}

func parseXML() {



	v := model.RootFiles{}

	data := `
<container xmlns="urn:oasis:names:tc:opendocument:xmlns:container" version="1.0">
			<rootfiles>
				<rootfile media-type="application/oebps-package+xml" full-path="content.opf"/>
			</rootfiles>
 </container>
		`
	//
	//


	err := xml.Unmarshal([]byte(data), &v)
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	//fmt.Printf("v = %#v\n", v)
	//v.Rootfiles/.

	fmt.Println(v.RootFiles.RootFile.FullPath)
}